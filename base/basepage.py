from datetime import datetime
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC


class BasePage():

    _popup_close_btn = (By.CSS_SELECTOR, "[class*='after-open Modal'] [class*='CloseButton']")

    def __init__(self, driver, base_url="https://www.savagex.com"):
        self.driver = driver
        self.base_url = base_url
        self.timeout = 30
        self.driver.get(base_url)

    def find_element(self, *loc):
        return self.driver.find_element(*loc)

    def find_elements(self, *loc):
        return self.driver.find_elements(*loc)

    def enter_txt(self, txt, *loc):
        return self.find_element(*loc).send_keys(txt)

    def wait_for_visible_elem(self, loc):
        try:
            return WebDriverWait(self.driver, 10).until(EC.visibility_of_element_located((loc)))
        except Exception as e:
            return False

    def checkif_elem_visible(self, *loc):
        if self.find_element(*loc).is_displayed():
            return True
        else:
            return False

    def get_time(self):
        curr_time = datetime.now()
        return curr_time.strftime("%m%d%y%H%M%S")

    def global_close_popup(self):
        if self.wait_for_visible_elem(self._popup_close_btn):
            self.find_element(*self._popup_close_btn).click()
