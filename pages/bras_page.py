from selenium.webdriver.common.by import By
from base.basepage import BasePage
import time


class BrasPage(BasePage):

    _first_grid_product = (By.CSS_SELECTOR, "[class*='ProductGrid__Container'] article:nth-child(1)")
    _flock_bra = (By.CSS_SELECTOR, "a[href='/products/6586822']")

    def clickFirstGridItem(self):
        self.driver.find_element(*self._first_grid_product).click()
        time.sleep(5)