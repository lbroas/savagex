from selenium.webdriver.common.by import By
from base.basepage import BasePage
import time


class OrderConfirmationPage(BasePage):

    _order_confirmation_container = (By.CSS_SELECTOR, "[class*='OrderConfirmationLayout__BeforeSummaryWrapper']")

    def verify_order_confirmation_displayed(self):
        self.wait_for_visible_elem(self._order_confirmation_container)