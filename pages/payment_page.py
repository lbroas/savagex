from selenium.webdriver.common.by import By
from base.basepage import BasePage
import time


class PaymentPage(BasePage):

    _cc_name_fld = (By.CSS_SELECTOR, "input[autocomplete='cc-name']")
    _cc_number_fld = (By.CSS_SELECTOR, "input[autocomplete='cc-number']")
    _cc_month_fld = (By.CSS_SELECTOR, "input[autocomplete='cc-exp-month']")
    _cc_year_fld = (By.CSS_SELECTOR, "input[autocomplete='cc-exp-year']")
    _cc_cvv_fld = (By.CSS_SELECTOR, "div:nth-child(5) input[class*='TextInput__Input']")
    _cc_continue_btn = (By.CSS_SELECTOR, "[data-autotag='edit-bill-save-btn']")

    _cc_name = "Testfirst Testlast"
    _cc_number = "4323565656565656"
    _cc_month = "12"
    _cc_year = "24"
    _cc_cvv = "123"

    def enter_cc_name(self):
        self.wait_for_visible_elem(self._cc_name_fld)
        self.enter_txt(self._cc_name, *self._cc_name_fld)

    def enter_cc_number(self):
        self.enter_txt(self._cc_number, *self._cc_number_fld)

    def enter_cc_month(self):
        self.enter_txt(self._cc_month, *self._cc_month_fld)

    def enter_cc_year(self):
        self.enter_txt(self._cc_year, *self._cc_year_fld)

    def enter_cc_cvv(self):
        self.enter_txt(self._cc_cvv, *self._cc_cvv_fld)

    def click_continue_btn(self):
        self.find_element(*self._cc_continue_btn).click()

    def enter_payment_info(self):
        self.enter_cc_name()
        self.enter_cc_number()
        self.enter_cc_month()
        self.enter_cc_year()
        self.enter_cc_cvv()
        self.click_continue_btn()