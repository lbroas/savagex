from selenium.webdriver.common.by import By
from base.basepage import BasePage
import random
import time


class PDPPage(BasePage):

    _pdp_size_dropdown = (By.CSS_SELECTOR, ".Select-arrow-zone")
    _pdp_size = (By.CSS_SELECTOR, ".Select-option")
    _pdp_addToCart_btn = (By.CSS_SELECTOR, "[class*='AddToCartButton']")

    def clickSizeDropDown(self):
        self.driver.find_element(*self._pdp_size_dropdown).click()
        time.sleep(2)

    def selectSize(self):
        self.driver.find_element(*self._pdp_size).click()

    def getNumSizes(self):
        count = (self.driver.find_elements(*self._pdp_size))
        sizes = []
        print("number of sizes: " + str(len(count)))
        for elem in count:
            if "Sold Out" not in elem.text:
                sizes.append(elem.text)
        randomSize = random.choice(sizes)
        self.driver.find_element_by_xpath("//*[@class='Select-menu']//*[contains(text(), '" + randomSize + "')]").click()
        print(randomSize)

    def clickAddToCart(self):
        self.driver.find_element(*self._pdp_addToCart_btn).click()
        time.sleep(3)