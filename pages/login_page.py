from selenium.webdriver.common.by import By
from base.basepage import BasePage
import time


class LoginPage(BasePage):

    _signin_link = (By.CSS_SELECTOR, "[class*='NavToolbar__SignInButton']")
    _email_fld = (By.CSS_SELECTOR, "input[data-autotag*='loginmodal-email-fld']")
    _password_fld = (By.CSS_SELECTOR, "input[data-autotag*='loginmodal-password-fld']")
    _signin_btn = (By.CSS_SELECTOR, "[data-autotag='loginmodal-btn'] [class*='Button__ButtonLabel']")
    _myaccount_btn = (By.CSS_SELECTOR, "[data-autotag='my-account']")
    _myaccount_logout_btn = (By.CSS_SELECTOR, "[data-autotag='logout-btn']")
    _topnav_bras = (By.CSS_SELECTOR, "[data-autotag='bras-main-menu']")
    _incorrect_email_pw_label = (By.CSS_SELECTOR, "[class*='TextInput__HelperText']")
    _acct_username = "tsqanonvip@test.com"
    _acct_pw_valid = "test12"
    _acct_pw_invalid = " "

    # ===== mobile =====
    _hamburger_btn = (By.CSS_SELECTOR, "[class*='NavMenuMobile__Container']")
    _m_sign_in_btn = (By.CSS_SELECTOR, "[data-autotag='sign-in']")

    #create account form
    _create_acct_email_fld = (By.CSS_SELECTOR, "input[data-autotag='reg-email-fld']")
    _create_acct_pw_fld = (By.CSS_SELECTOR, "input[data-autotag='reg-password-fld']")
    _create_account_btn = (By.CSS_SELECTOR, "[class*='SignIn__SecondaryButton']")
    _submit_account_btn = (By.CSS_SELECTOR, "[data-autotag='reg-continue-btn']")


    def click_signin_link(self):
        self.wait_for_visible_elem(self._signin_link)
        self.driver.find_element(*self._signin_link).click()

    def signin_enter_email(self, email):
        self.driver.find_element(*self._email_fld).send_keys(email)

    def signin_enter_password(self, password):
        self.driver.find_element(*self._password_fld).send_keys(password)

    def click_signin_btn(self):
        self.driver.find_element(*self._signin_btn).click()


    def verify_login_successful(self):
        myacct = self.wait_for_visible_elem(self._myaccount_btn)
        myacct.is_displayed()

    def click_create_acct_btn(self):
        self.wait_for_visible_elem(self._create_account_btn)
        self.find_element(*self._create_account_btn).click()

    def create_acct_enter_email(self):
        self.wait_for_visible_elem(self._create_acct_email_fld)
        self.enter_txt(("autotest" + self.get_time() + "@test.com"), *self._create_acct_email_fld)

    def create_acct_enter_pw(self):
        self.enter_txt(self._acct_pw_valid, *self._create_acct_pw_fld)

    def submit_create_acct_btn(self):
        self.find_element(*self._submit_account_btn).click()

    def signin_invalid_acct(self):
        self.wait_for_visible_elem(self._signin_link)
        self.global_close_popup()
        self.find_element(*self._signin_link).click()
        self.signin_enter_email(self._acct_username)
        self.signin_enter_password(self._acct_pw_invalid)
        self.click_signin_btn()
        error_label = self.wait_for_visible_elem(self._incorrect_email_pw_label)
        error_label.is_displayed()

    def signin_valid_acct(self):
        self.wait_for_visible_elem(self._signin_link)
        self.global_close_popup()
        self.find_element(*self._signin_link).click()
        self.signin_enter_email(self._acct_username)
        self.signin_enter_password(self._acct_pw_valid)
        self.click_signin_btn()

    def click_logout(self):
        self.wait_for_visible_elem(self._myaccount_btn)
        self.find_element(*self._myaccount_btn).click()
        self.find_element(*self._myaccount_logout_btn).click()
        self.wait_for_visible_elem(self._signin_link)

    # ===== mobile methods =====
    def click_m_hamburger_nav(self):
        self.wait_for_visible_elem(self._hamburger_btn)
        self.find_element(*self._hamburger_btn).click()

    def click_m_sign_in_btn(self):
        self.wait_for_visible_elem(self._m_sign_in_btn)
        self.find_element(*self._m_sign_in_btn).click()


    def clickBras(self):
        self.wait_for_visible_elem(self._topnav_bras)
        self.driver.find_element(*self._topnav_bras).click()

    def create_acct(self):
        self.click_signin_link()
        self.click_create_acct_btn()
        self.create_acct_enter_email()
        self.create_acct_enter_pw()
        self.submit_create_acct_btn()
        self.verify_login_successful()

    def m_create_acct(self):
        self.click_m_hamburger_nav()
        self.click_m_sign_in_btn()

    def goToBras(self):
        self.clickBras()
        time.sleep(5)
