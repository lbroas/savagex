from selenium.webdriver.common.by import By
from base.basepage import BasePage

class PlaceOrderPage(BasePage):


        _place_order_btn = (By.CSS_SELECTOR, "[class*='ViewOrderSummaryLayout__PlaceOrderButton'] [class*='Button']")


        def click_place_order(self):
            self.wait_for_visible_elem(self._place_order_btn)
            self.find_element(*self._place_order_btn).click()