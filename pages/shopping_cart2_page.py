from selenium.webdriver.common.by import By
from base.basepage import BasePage


class ShoppingCart2Page(BasePage):

    _checkout_btn = (By.CSS_SELECTOR, ".bfx-checkout")

    def clickCheckout(self):
        self.wait_for_visible_elem(self._checkout_btn)
        self.driver.find_element(*self._checkout_btn).click()

