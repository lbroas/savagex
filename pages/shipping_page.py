from selenium.webdriver.common.by import By
from base.basepage import BasePage
import time

class ShippingPage(BasePage):
    _fname_field = (By.CSS_SELECTOR, "input[autocomplete='given-name']")
    _lname_field = (By.CSS_SELECTOR, "input[autocomplete='family-name']")
    _address_field = (By.CSS_SELECTOR, "input[autocomplete='address-line1']")
    _city_field = (By.CSS_SELECTOR, "input[autocomplete='city']")
    _state_dropdown = (By.CSS_SELECTOR, "div:nth-child(6) [class*='Select__SelectElement']:nth-of-type(1)")
    _zip_code_fld = (By.CSS_SELECTOR, "input[autocomplete='postal-code']")
    _phone_fld = (By.CSS_SELECTOR, "input[autocomplete='tel']")
    _continue_btn = (By.CSS_SELECTOR, "[class*='ShippingInfoLayout__ContinueButton']")
    _california = (By.CSS_SELECTOR, "[class*='Select__SelectElement'] [value='CA']")


    def enter_first_name(self, fname):
        self.wait_for_visible_elem(self._fname_field)
        self.find_element(*self._fname_field).send_keys(fname)

    def enter_last_name(self, lname):
        self.find_element(*self._lname_field).send_keys(lname)

    def enter_address(self, address):
        self.find_element(*self._address_field).send_keys(address)

    def select_state(self):
        self.find_element(*self._state_dropdown).click()
        self.find_element(*self._california).click()

    def enter_city(self, city):
        self.find_element(*self._city_field).send_keys(city)

    def enter_postal_code(self, postal):
        self.find_element(*self._zip_code_fld).send_keys(postal)

    def enter_phone_number(self, phone):
        self.find_element(*self._phone_fld).send_keys(phone)


    def fill_out_shipping_form(self):
        self.enter_first_name("testfirst")
        self.enter_last_name("testlast")
        self.enter_address("800 Apollo St")
        self.select_state()
        self.enter_city("El Segundo")
        self.enter_postal_code("90245")
        self.enter_phone_number("310-555-1212")
        self.click_continue_btn()

    def click_continue_btn(self):
        self.find_element(*self._continue_btn).click()
        time.sleep(5)