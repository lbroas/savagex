from selenium.webdriver.common.by import By
from base.basepage import BasePage


class ShoppingCart1Page(BasePage):

    _checkout_btn = (By.CSS_SELECTOR, "[class*='CartDrawer__CheckoutButton']")

    def clickCheckout(self):
        self.wait_for_visible_elem(self._checkout_btn)
        self.driver.find_element(*self._checkout_btn).click()
