import pytest
from pages.login_page import LoginPage
import unittest

@pytest.mark.usefixtures("mobile_setup")
class mTestLogin(unittest.TestCase):

    @pytest.fixture(autouse=True)
    def object_setup(self, mobile_setup):
        self.lp = LoginPage(self.driver)


    def test_login(self):
        self.lp.click_m_hamburger_nav()
        self.lp.click_m_sign_in_btn()