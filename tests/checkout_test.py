import pytest
from pages.login_page import LoginPage
from pages.bras_page import BrasPage
from pages.pdp_page import PDPPage
from pages.shopping_cart1_page import ShoppingCart1Page
from pages.shopping_cart2_page import ShoppingCart2Page
from pages.shipping_page import ShippingPage
from pages.payment_page import PaymentPage
from pages.place_order_page import PlaceOrderPage
from pages.order_confirmation_page import OrderConfirmationPage

import unittest

@pytest.mark.usefixtures("oneTimeSetUp")
class TestCheckout(unittest.TestCase):

    @pytest.fixture(autouse=True)
    def objectSetup(self, oneTimeSetUp):
        self.lp = LoginPage(self.driver)
        self.bras = BrasPage(self.driver)
        self.pdp = PDPPage(self.driver)
        self.cart1 = ShoppingCart1Page(self.driver)
        self.cart2 = ShoppingCart2Page(self.driver)
        self.shipping = ShippingPage(self.driver)
        self.payment = PaymentPage(self.driver)
        self.order = PlaceOrderPage(self.driver)
        self.confirm = OrderConfirmationPage(self.driver)

    def test_checkout(self):
        self.lp.create_acct()
        self.lp.goToBras()
        self.bras.clickFirstGridItem()
        self.pdp.clickSizeDropDown()
        self.pdp.getNumSizes()
        self.pdp.clickAddToCart()

        self.cart1.clickCheckout()
        self.cart2.clickCheckout()
        self.shipping.fill_out_shipping_form()

        self.payment.enter_payment_info()
        self.order.click_place_order()
        self.confirm.verify_order_confirmation_displayed()