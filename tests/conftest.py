import pytest
from selenium import webdriver


@pytest.fixture(scope="class")
def oneTimeSetUp(request):
    print("Running one time setUp")
    driver = webdriver.Chrome()

    if request.cls is not None:
        request.cls.driver = driver

    yield driver
    driver.quit()
    print("Running one time tearDown")


@pytest.fixture(scope="class")
def mobile_setup(request):
    print("Setting up mobile test")
    mobile_emulation = {"deviceName": "iPhone 6"}
    chrome_options = webdriver.ChromeOptions()
    chrome_options.add_experimental_option("mobileEmulation", mobile_emulation)
    driver = webdriver.Chrome(chrome_options=chrome_options)

    if request.cls is not None:
        request.cls.driver = driver

    yield driver
    driver.quit()
    print("Running teardown for mobile")

# def pytest_adoption(parser):
#     parser.adoption("--browser")
#
# @pytest.fixture(scope="session")
# def browser(request):
#     return request.config.getoption("--browser")