import pytest
from pages.login_page import LoginPage


import unittest

@pytest.mark.usefixtures("oneTimeSetUp")
class TestLogin(unittest.TestCase):


    @pytest.fixture(autouse=True)
    def objectSetup(self, oneTimeSetUp):
        self.lp = LoginPage(self.driver)

    @pytest.mark.run(order=1)
    def test_invalid_credentials(self):
        self.lp.signin_invalid_acct()

    @pytest.mark.run(order=2)
    def test_valid_credentials(self):
        self.lp.signin_valid_acct()
        self.lp.click_logout()

    @pytest.mark.run(order=3)
    def test_create_acct(self):
        self.lp.create_acct()
        self.lp.click_logout()

